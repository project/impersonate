Development of this module has been financed by WorthPoint Inc. (http://www.worthpoint.com/)

Description
-----------
This module provides abillity to impersonate another user without relogging.


Installation
------------
To install, copy the impersonate directory and all its contents to your modules
directory.


Configuration
-------------
To enable this module, visit Administer -> Site building -> Modules, and
enable Impersonate.

Configuration consists of several steps:
a) Administrator part
1. Go to Administer -> User Management -> Access Control -> impersonate module and set roles that will have 'administer impersonate module', and other roles that will be able to actually impersonate by setting 'impersonate user' right.

2. Login as a user that has 'administer impersonate module' right and go the specific user account that has assigned role with 'impersonate user' right (Visit Administer -� User management -> Users and click on edit link near desired account).

3. You will see tab "Impersonate Administration". Click on it and set roles for this user. Setting roles means that that specific user will be able to impersonate any user on the system that has at least one role from the list of selected. This can be a huge security breach so be very careful how you use it.

b) Impersonator part
1. Login to the system and go to edit your account. You will see the tab "Impersonate". Click on it and you will be presented with field to enter username you wish to impersonate. That field has autocomplete that will help you locate username you are searching for. After you typed the name you want just click on Impersonate button. Instatly you become that user.

2. To restore to your original user go to edit account again and click on Impersonate tab. Now you will see just big button saying "Reset Impersonation". Click on it and you are back to your old account. 


Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do
so at the project page on the Drupal web site.
http://drupal.org/project/impersonate


Author
------
Darko Miletic (UVCMS e-learning http://www.uvcms.com/)

If you use this module, find it useful, and want to send the author a thank
you note, then use the Feedback/Contact page at the URL above.


